package com.dicoding.assosiate.tugastiga;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import static android.provider.BaseColumns._ID;
import static com.dicoding.assosiate.tugastiga.DatabaseContract.KamusColumns.*;
import static com.dicoding.assosiate.tugastiga.DatabaseContract.TABLE_IND_TO_ENG;
import static com.dicoding.assosiate.tugastiga.DatabaseContract.TABLE_ENG_TO_IND;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME         = "dbkamus";
    private static final int DATABASE_VERSION   = 1;

    public static String CREATE_TABLE_IND_TO_ENG = "create table "+TABLE_IND_TO_ENG+
            " ("+_ID+" integer primary key autoincrement, " +
            FIELD_KATA+" text not null, " +
            FIELD_ARTI+" text not null);";

    public static String CREATE_TABLE_ENG_TO_IND = "create table "+TABLE_ENG_TO_IND+
            " ("+_ID+" integer primary key autoincrement, " +
            FIELD_KATA+" text not null, " +
            FIELD_ARTI+" text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_IND_TO_ENG);
        sqLiteDatabase.execSQL(CREATE_TABLE_ENG_TO_IND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_IND_TO_ENG);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_ENG_TO_IND);
        onCreate(sqLiteDatabase);
    }
}
